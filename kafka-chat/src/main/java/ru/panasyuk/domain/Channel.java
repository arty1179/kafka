package ru.panasyuk.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 */
@Entity(name="Channel")
@Table(name="channel")
public class Channel implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    public Channel() {
    }

    public Channel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Channel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
