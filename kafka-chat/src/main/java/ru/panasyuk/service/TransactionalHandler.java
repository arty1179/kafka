package ru.panasyuk.service;

import ru.panasyuk.dao.HibernateSessionFactoryUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class TransactionalHandler {

    EntityManager em = HibernateSessionFactoryUtil.getEntityManager();

    void doTransaction(TransactionalOperation transactionalOperation) {
        // Create transaction
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            transactionalOperation.doOperation(em);
        } catch (Exception ex) {
            tx.rollback();
            throw ex;
        }
        tx.commit();
    }
}