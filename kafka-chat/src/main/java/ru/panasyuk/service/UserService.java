package ru.panasyuk.service;

import ru.panasyuk.dao.UserDao;
import ru.panasyuk.domain.User;

import javax.persistence.EntityManager;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.List;
import java.util.logging.Logger;

public class UserService {
    private static Logger log = Logger.getLogger(UserService.class.getName());

    private UserDao userDao = new UserDao();

    private static Validator validator;
    static {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.usingContext().getValidator();
    }

    private TransactionalHandler transactionalHandler = new TransactionalHandler();

    public void addNewUserTransact(User user) {
        transactionalHandler.doTransaction((em) -> {
            addNewUser(user, em);
        });
    }

    public List<User> findUsers() {
        return userDao.findUsers();
    }

    public User findUserById(long id){
        return userDao.findUserById(id);
    }

    public void addNewUser(User user, EntityManager em) {
        //validator.validate(user);
        userDao.save(user, em);
        log.info(String.format("user with channels=%s has been added",
                user.getChannels()));
    }

    public void updateUser(User user) {
        userDao.update(user);
        log.info(String.format("user with channels=%s has been updated",
                user.getChannels()));
    }

    public void deleteUser(User user) {
        userDao.delete(user);
        log.info(String.format("user with channels=%s has been deleted",
                user.getChannels()));
    }
}
