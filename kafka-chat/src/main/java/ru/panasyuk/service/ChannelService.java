package ru.panasyuk.service;

import ru.panasyuk.dao.ChannelDao;
import ru.panasyuk.domain.Channel;

import java.util.List;
import java.util.logging.Logger;

public class ChannelService {
    private static Logger log = Logger.getLogger(ChannelService.class.getName());

    private ChannelDao channelDao = new ChannelDao();

    public List<Channel> findChannels() {
        return channelDao.findChannels();
    }

    public Channel findChannelById(long id){
        return channelDao.findChannelById(id);
    }

    public void addNewChannel(Channel channel) {
        channelDao.save(channel);
        log.info(String.format("channel=%s has been added",
                channel));
    }
    public void updateUser(Channel channel) {
        channelDao.update(channel);
        log.info(String.format("channel=%s has been updated channel",
                channel));
    }

    public void deleteUser(Channel channel) {
        channelDao.delete(channel);
        log.info(String.format("channel=%s has been deleted",
                channel));
    }

}
