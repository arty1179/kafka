package ru.panasyuk.message;

public class Message implements Comparable<Message>{
    private String sender;
    private String text;
    private long timestamp;

    public Message(String sender, String text) {
        this.sender = sender;
        this.text = text;
        this.timestamp = System.currentTimeMillis();
    }

    public String getSender() {
        return sender;
    }

    public String getText() {
        return text;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int compareTo(Message o) {
        return (this.timestamp<=o.getTimestamp())?-1:1;
    }
}
