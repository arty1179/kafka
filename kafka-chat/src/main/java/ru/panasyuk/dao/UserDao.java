package ru.panasyuk.dao;

import ru.panasyuk.domain.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class UserDao {

    public List<User> findUsers() {
        EntityManager em = HibernateSessionFactoryUtil.getEntityManager();
        Query query = em.createNamedQuery("findAll");
        List<User> user = query.getResultList();
        return user;
    }

    public User findUserById(long id) {
        EntityManager em = HibernateSessionFactoryUtil.getEntityManager();
        Query query = em.createNamedQuery("findById")
                .setParameter("id", id);
        User user = (User) query.getSingleResult();
        return user;
    }

    public void save(User user, EntityManager em) {
        if (user.getId() == null) {
            em.persist(user);
        } else {
            em.merge(user);
        }
    }

    public void update(User user) {
        EntityManager em = HibernateSessionFactoryUtil.getEntityManager();
        EntityTransaction tx = em.getTransaction();
        em.merge(user);
        tx.commit();
        em.close();
    }

    public void delete(User user) {
        EntityManager em = HibernateSessionFactoryUtil.getEntityManager();
        EntityTransaction tx = em.getTransaction();
        em.remove(user);
        tx.commit();
        em.close();
    }
}
