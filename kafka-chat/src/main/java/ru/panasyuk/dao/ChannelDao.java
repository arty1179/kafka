package ru.panasyuk.dao;

import ru.panasyuk.domain.Channel;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class ChannelDao {


    public List<Channel> findChannels() {
        EntityManager em = HibernateSessionFactoryUtil.getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Channel> criteriaQuery = builder.createQuery(Channel.class);
        Root<Channel> c = criteriaQuery.from(Channel.class);
        criteriaQuery.select(c);
        Query query = em.createQuery(criteriaQuery);
        List<Channel> channel = query.getResultList();
        return channel;
    }

    public Channel findChannelById(long id) {
        EntityManager em = HibernateSessionFactoryUtil.getEntityManager();
        Channel ch = em.find(Channel.class, id);
        return ch;
    }

    public void save(Channel channel) {
        EntityManager em = HibernateSessionFactoryUtil.getEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        if (channel.getId() == null) {
            em.persist(channel);
        } else {
            em.merge(channel);
        }
        tx.commit();
        em.close();
    }

    public void update(Channel channel) {
        EntityManager em = HibernateSessionFactoryUtil.getEntityManager();
        EntityTransaction tx = em.getTransaction();
        em.merge(channel);
        tx.commit();
        em.close();
    }

    public void delete(Channel channel) {
        EntityManager em = HibernateSessionFactoryUtil.getEntityManager();
        EntityTransaction tx = em.getTransaction();
        em.remove(channel);
        tx.commit();
        em.close();
    }
}
