import ru.panasyuk.domain.Channel;
import ru.panasyuk.domain.User;
import ru.panasyuk.service.ChannelService;
import ru.panasyuk.service.UserService;

import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        UserService userService = new UserService();
        ChannelService channelService = new ChannelService();

        List<Channel> channelList = new ArrayList<>();
        Channel ch1 = new Channel("ch1");
        Channel ch2 = new Channel("ch2");
        Channel ch3 = new Channel("ch3");
        Channel ch4 = new Channel("ch4");
        //channelService.addNewChannel(ch1);
        //channelService.addNewChannel(ch2);
        //channelService.addNewChannel(ch3);
        //channelService.addNewChannel(ch4);
        System.out.println(channelService.findChannelById(11L));
        System.out.println("All channels: " + channelService.findChannels());
        channelList.add(ch3);
        channelList.add(ch4);
        User user = new User();
        user.setNickName("NotValidTestUser");
        user.setChannels(channelList);
        //userService.addNewUser(user);
        userService.addNewUserTransact(user);
        System.out.println(userService.findUserById(33L));
        System.out.println("All users: " + userService.findUsers());
    }
}
