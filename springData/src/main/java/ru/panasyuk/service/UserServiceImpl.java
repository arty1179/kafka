package ru.panasyuk.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.panasyuk.dao.UserRepo;
import ru.panasyuk.domain.User;

import java.util.List;
import java.util.logging.Logger;
@Service("springJpaUserService")
@Repository
@Transactional
public class UserServiceImpl implements UserService {
    private static Logger log = Logger.getLogger(UserServiceImpl.class.getName());
    @Autowired
    private UserRepo userRepo;

    @Override
    @Transactional(readOnly=true)
    public List<User> findUsers() {
        return (List<User>) userRepo.findAll();
    }

    @Override
    @Transactional(readOnly=true)
    public User findUserById(long id){
        return userRepo.findOne(id);
    }

    @Override
    @Transactional
    public void saveUser(User user) {
        userRepo.save(user);
        log.info(String.format("user with channels=%s has been added",
                user.getChannels()));
    }

    @Override
    @Transactional
    public void deleteUser(User user) {
        userRepo.delete(user);
        log.info(String.format("user with channels=%s has been deleted",
                user.getChannels()));
    }
}
