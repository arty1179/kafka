package ru.panasyuk.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.panasyuk.dao.ChannelRepo;
import ru.panasyuk.domain.Channel;

import java.util.List;
import java.util.logging.Logger;
@Service("springJpaChannelService")
@Repository
@Transactional
public class ChannelServiceImpl implements ChannelService{
    private static Logger log = Logger.getLogger(ChannelServiceImpl.class.getName());

    @Autowired
    private ChannelRepo channelRepo;

    @Override
    @Transactional(readOnly=true)
    public List<Channel> findChannels() {
        return (List<Channel>) channelRepo.findAll();
    }

    @Override
    @Transactional(readOnly=true)
    public Channel findChannelById(long id){
        return channelRepo.findOne(id);
    }

    @Override
    @Transactional
    public void addNewChannel(Channel channel) {
        channelRepo.save(channel);
        log.info(String.format("channel=%s has been added",
                channel));
    }

    @Override
    @Transactional
    public void updateChannel(Channel channel) {
        channelRepo.save(channel);
        log.info(String.format("channel=%s has been updated channel",
                channel));
    }

    @Override
    @Transactional
    public void deleteChannel(Channel channel) {
        channelRepo.delete(channel);
        log.info(String.format("channel=%s has been deleted",
                channel));
    }

}
