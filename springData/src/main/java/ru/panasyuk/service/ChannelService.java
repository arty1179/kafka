package ru.panasyuk.service;

import ru.panasyuk.domain.Channel;

import java.util.List;

public interface ChannelService {
    List<Channel> findChannels();

    Channel findChannelById(long id);

    void addNewChannel(Channel channel);

    void updateChannel(Channel channel);

    void deleteChannel(Channel channel);
}
