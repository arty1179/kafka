package ru.panasyuk.service;

import org.springframework.transaction.annotation.Transactional;
import ru.panasyuk.domain.User;

import java.util.List;

public interface UserService {
    @Transactional(readOnly=true)
    List<User> findUsers();

    @Transactional(readOnly=true)
    User findUserById(long id);

    @Transactional
    void saveUser(User user);

    @Transactional
    void deleteUser(User user);
}