package ru.panasyuk.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
@Entity(name = "User")
@NamedQueries({
        @NamedQuery(name = "findAll", query="select u from User u"),
        @NamedQuery(name = "findById", query="select u from User u where u.id = :id")
}
)
@Table(name = "user",  schema = "public")
public class User implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;

    private String nickName;
    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinTable(name = "user_channel",
            joinColumns = @JoinColumn(name = "user_fk"),
            inverseJoinColumns = @JoinColumn(name = "channel_fk"))
    private List<Channel> channels;

    public User() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Channel> getChannels() {
        return channels;
    }

    public void setChannels(List<Channel> channels) {
        this.channels = channels;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", nickName='" + nickName + '\'' +
                ", channels=" + channels +
                '}';
    }
}
