package ru.panasyuk;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import ru.panasyuk.config.ApplicationConfig;
import ru.panasyuk.domain.Channel;
import ru.panasyuk.domain.User;
import ru.panasyuk.service.ChannelService;
import ru.panasyuk.service.UserService;

import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        //GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        //ctx.load("classpath:META-INF/spring/app-context-annotation.xml");
        ApplicationContext ctx = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        //ctx.refresh();

        UserService userService = ctx.getBean(
                "springJpaUserService", UserService.class);
        ChannelService channelService = ctx.getBean(
                "springJpaChannelService", ChannelService.class);

        List<Channel> channelList = new ArrayList<>();
        Channel ch1 = new Channel("ch1");
        Channel ch2 = new Channel("ch2");
        Channel ch3 = new Channel("ch3");
        Channel ch4 = new Channel("ch4");
        channelService.updateChannel(ch1);
        channelService.updateChannel(ch2);
        channelList.add(channelService.findChannelById(4L));
        System.out.println("All channels: " + channelService.findChannels());
        channelList.add(ch3);
        channelList.add(ch4);
        User user = new User();
        user.setNickName("NotValidTestUser");
        user.setChannels(channelList);
        userService.saveUser(user);
        //System.out.println(userService.findUserById(33L));
        //System.out.println("All users: " + userService.findUsers());
    }
}
