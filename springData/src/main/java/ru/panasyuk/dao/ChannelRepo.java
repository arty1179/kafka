package ru.panasyuk.dao;

import org.springframework.data.repository.CrudRepository;
import ru.panasyuk.domain.Channel;


public interface ChannelRepo extends CrudRepository<Channel,Long> {
}
