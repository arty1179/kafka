package ru.panasyuk.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.panasyuk.domain.User;

@Repository
public interface UserRepo extends CrudRepository<User,Long> {
}
