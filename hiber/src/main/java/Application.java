import ru.panasyuk.domain.ApplicationSettings;
import ru.panasyuk.domain.DeveloperSettings;
import ru.panasyuk.service.AppService;
import ru.panasyuk.service.DevService;

public class Application {
    public static void main(String[] args) {
        DevService devService = new DevService();
        AppService appService = new AppService();
        ApplicationSettings ch1 = new ApplicationSettings("256", "Linux");
        ApplicationSettings ch2 = new ApplicationSettings("512", "Unix");
        ApplicationSettings ch3 = new ApplicationSettings("1024", "MacOS");
        ApplicationSettings ch4 = new ApplicationSettings("2048", "Windows");
        appService.addNewAppSettings(ch1);
        appService.addNewAppSettings(ch2);
        //channelService.addNewChannel(ch3);
        //channelService.addNewChannel(ch4);
        System.out.println(appService.findAppSettingsById(11L));

        DeveloperSettings developerSettings = new DeveloperSettings();
        developerSettings.setEmail("arty@sdwd.ru");
        developerSettings.setFio("Ivan Petrov");

        devService.addDevSettingsTransact(developerSettings);
        System.out.println(devService.findDevSettingsById(33L));
        System.out.println("All dev settings: " + devService.findDevSettings());
    }
}
