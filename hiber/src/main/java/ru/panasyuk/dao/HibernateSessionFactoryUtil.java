package ru.panasyuk.dao;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class HibernateSessionFactoryUtil {
    public static EntityManager getEntityManager() {
        final EntityManager em =
                Persistence.createEntityManagerFactory("sample").createEntityManager();
        return em;
    }
}
