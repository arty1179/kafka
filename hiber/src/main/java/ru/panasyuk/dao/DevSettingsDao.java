package ru.panasyuk.dao;

import ru.panasyuk.domain.DeveloperSettings;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class DevSettingsDao {

    public List<DeveloperSettings> findDevSettings() {
        EntityManager em = HibernateSessionFactoryUtil.getEntityManager();
        Query query = em.createNamedQuery("findAllAppSettings");
        List<DeveloperSettings> developerSettings = query.getResultList();
        return developerSettings;
    }

    public DeveloperSettings findDevSettingsById(long id) {
        EntityManager em = HibernateSessionFactoryUtil.getEntityManager();
        DeveloperSettings developerSettings = em.find(DeveloperSettings.class, id);
        return developerSettings;
    }

    public void save(DeveloperSettings developerSettings, EntityManager em) {
        if (developerSettings.getId() == null) {
            em.persist(developerSettings);
        } else {
            em.merge(developerSettings);
        }
    }

    public void update(DeveloperSettings developerSettings) {
        EntityManager em = HibernateSessionFactoryUtil.getEntityManager();
        EntityTransaction tx = em.getTransaction();
        em.merge(developerSettings);
        tx.commit();
        em.close();
    }

    public void delete(DeveloperSettings developerSettings) {
        EntityManager em = HibernateSessionFactoryUtil.getEntityManager();
        EntityTransaction tx = em.getTransaction();
        em.remove(developerSettings);
        tx.commit();
        em.close();
    }
}
