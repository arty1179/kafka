package ru.panasyuk.dao;

import ru.panasyuk.domain.ApplicationSettings;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class AppSettingsDao {


    public List<ApplicationSettings> findAppSettings() {
        EntityManager em = HibernateSessionFactoryUtil.getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<ApplicationSettings> criteriaQuery = builder.createQuery(ApplicationSettings.class);
        Root<ApplicationSettings> c = criteriaQuery.from(ApplicationSettings.class);
        criteriaQuery.select(c);
        Query query = em.createQuery(criteriaQuery);
        List<ApplicationSettings> applicationSettings = query.getResultList();
        return applicationSettings;
    }

    public ApplicationSettings findAppSettingsById(long id) {
        EntityManager em = HibernateSessionFactoryUtil.getEntityManager();
        ApplicationSettings ch = em.find(ApplicationSettings.class, id);
        return ch;
    }

    public void save(ApplicationSettings applicationSettings) {
        EntityManager em = HibernateSessionFactoryUtil.getEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        if (applicationSettings.getId() == null) {
            em.persist(applicationSettings);
        } else {
            em.merge(applicationSettings);
        }
        tx.commit();
        em.close();
    }

    public void update(ApplicationSettings applicationSettings) {
        EntityManager em = HibernateSessionFactoryUtil.getEntityManager();
        EntityTransaction tx = em.getTransaction();
        em.merge(applicationSettings);
        tx.commit();
        em.close();
    }

    public void delete(ApplicationSettings applicationSettings) {
        EntityManager em = HibernateSessionFactoryUtil.getEntityManager();
        EntityTransaction tx = em.getTransaction();
        em.remove(applicationSettings);
        tx.commit();
        em.close();
    }
}
