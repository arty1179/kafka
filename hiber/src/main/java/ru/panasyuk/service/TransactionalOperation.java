package ru.panasyuk.service;

import javax.persistence.EntityManager;

public interface TransactionalOperation {

    void doOperation(EntityManager em);

}
