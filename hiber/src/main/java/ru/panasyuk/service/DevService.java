package ru.panasyuk.service;

import ru.panasyuk.dao.DevSettingsDao;
import ru.panasyuk.domain.DeveloperSettings;

import javax.persistence.EntityManager;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.List;
import java.util.logging.Logger;

public class DevService {
    private static Logger log = Logger.getLogger(DevService.class.getName());

    private DevSettingsDao userDao = new DevSettingsDao();

    private static Validator validator;
    static {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.usingContext().getValidator();
    }

    private TransactionalHandler transactionalHandler = new TransactionalHandler();

    public void addDevSettingsTransact(DeveloperSettings developerSettings) {
        transactionalHandler.doTransaction((em) -> {
            addNewDevSettings(developerSettings, em);
        });
    }

    public List<DeveloperSettings> findDevSettings() {
        return userDao.findDevSettings();
    }

    public DeveloperSettings findDevSettingsById(long id){
        return userDao.findDevSettingsById(id);
    }

    public void addNewDevSettings(DeveloperSettings developerSettings, EntityManager em) {
        //validator.validate(user);
        userDao.save(developerSettings, em);
        log.info(String.format("devSettings  has been added",
                developerSettings));
    }

    public void updateUser(DeveloperSettings developerSettings) {
        userDao.update(developerSettings);
        log.info(String.format("devSettings has been updated",
                developerSettings));
    }

    public void deleteUser(DeveloperSettings developerSettings) {
        userDao.delete(developerSettings);
        log.info(String.format("devSettings has been deleted",
                developerSettings));
    }
}
