package ru.panasyuk.service;

import ru.panasyuk.dao.AppSettingsDao;
import ru.panasyuk.domain.ApplicationSettings;

import java.util.List;
import java.util.logging.Logger;

public class AppService {
    private static Logger log = Logger.getLogger(AppService.class.getName());

    private AppSettingsDao appSettingsDao = new AppSettingsDao();

    public List<ApplicationSettings> findAppSettings() {
        return appSettingsDao.findAppSettings();
    }

    public ApplicationSettings findAppSettingsById(long id){
        return appSettingsDao.findAppSettingsById(id);
    }

    public void addNewAppSettings(ApplicationSettings applicationSettings) {
        appSettingsDao.save(applicationSettings);
        log.info(String.format("channel=%s has been added",
                applicationSettings));
    }
    public void updateUser(ApplicationSettings applicationSettings) {
        appSettingsDao.update(applicationSettings);
        log.info(String.format("channel=%s has been updated channel",
                applicationSettings));
    }

    public void deleteUser(ApplicationSettings applicationSettings) {
        appSettingsDao.delete(applicationSettings);
        log.info(String.format("channel=%s has been deleted",
                applicationSettings));
    }

}
