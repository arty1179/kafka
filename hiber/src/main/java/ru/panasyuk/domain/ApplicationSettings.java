package ru.panasyuk.domain;

import javax.persistence.*;

/**
 *
 */
@Entity
@DiscriminatorValue("A")
@NamedQueries({
        @NamedQuery(name = "findAllAppSettings", query="select u from ApplicationSettings u"),
        @NamedQuery(name = "findAppSettingsById", query="select u from ApplicationSettings u where u.id = :id")
}
)
public class ApplicationSettings extends Settings {

    private String memorySize;

    private String operationSystem;

    public ApplicationSettings() {
    }

    public ApplicationSettings(String memorySize, String operationSystem) {
        this.memorySize = memorySize;
        this.operationSystem = operationSystem;
    }

    public String getOperationSystem() {
        return operationSystem;
    }

    public void setOperationSystem(String operationSystem) {
        this.operationSystem = operationSystem;
    }

    public String getMemorySize() {
        return memorySize;
    }

    public void setMemorySize(String name) {
        this.memorySize = name;
    }

    @Override
    public String toString() {
        return "ApplicationSettings{" +
                "memorySize='" + memorySize + '\'' +
                ", operationSystem='" + operationSystem + '\'' +
                '}';
    }
}
