package ru.panasyuk.domain;

import javax.persistence.*;

/**
 *
 */
@Entity
@DiscriminatorValue("D")
@NamedQueries({
        @NamedQuery(name = "findAllDevSettings", query="select u from DeveloperSettings u"),
        @NamedQuery(name = "findDevSettingById", query="select u from DeveloperSettings u where u.id = :id")
}
)
public class DeveloperSettings extends Settings {

    private String fio;

    private String email;

    public DeveloperSettings() {
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String nickName) {
        this.email = nickName;
    }

    @Override
    public String toString() {
        return "DeveloperSettings{" +
                "fio='" + fio + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
