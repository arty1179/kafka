package ru.innopolis.stc12.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import ru.innopolis.stc12.spring.db.dao.StudentRepo;
import ru.innopolis.stc12.spring.db.pojo.Student;

import java.util.List;

@Service
@Repository
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentRepo studentRepo;

    @Override
    public List<Student> getStudentsList() {
        return (List<Student>) studentRepo.findAll();
    }

    @Override
    public void addStudent(String name, String familyName, String age, String group) {
        Student student = new Student(
                name,
                familyName,
                Integer.valueOf(age),
                group
        );
        studentRepo.save(student);
    }
}
