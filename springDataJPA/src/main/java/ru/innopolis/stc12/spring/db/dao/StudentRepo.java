package ru.innopolis.stc12.spring.db.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.innopolis.stc12.spring.db.pojo.Student;


public interface StudentRepo extends CrudRepository<Student,Long> {
}
