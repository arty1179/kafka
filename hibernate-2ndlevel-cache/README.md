## How to Run

```
mvn exec:java -Dexec.mainClass="com.hazelcast.hibernate.EntityCache" - проверяем кеширование сущности
mvn exec:java -Dexec.mainClass="com.hazelcast.hibernate.CollectionCache" - проверяем кэширование сущности с коллекцией
```

mvn exec:java -Dexec.mainClass="com.hazelcast.hibernate.StartInstance"
```
Команда запускает ноды кэша, чем больше раз запускаем тем больше нод