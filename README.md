## How to Run
0.Поднять сервер БД (Postgres v12+)
 в docker контейнере с помощью команды docker-compose up -d в корневой папке проекта.	 
1. Start `ZooKeeper` server on your machine

2. Start `Kafka` server on your machine

2. Run `ChatServer` from the generated `jar` in `target` folder  

	 ```
	 $ java -cp target/kafka-chat-1.0-jar-with-dependencies.jar chat.ChatServer

	 ```
3. Run `ChatClient` from the generated `jar` in `target` folder  

	 ```
	 $ java -cp target/kafka-chat-1.0-jar-with-dependencies.jar chat.ChatClient
	 ```

## Chat Commands
- `nick <nickname>` : login as `nickname`. Leave `nickname` empty to login as a random user
- `join <channelname>` : join to a channel named `channelname`
- `leave <channelname>` : leave a channel named `channelname`
- `@<channelname> <ru.panasyuk.message>` :  send `ru.panasyuk.message` to a channel named `channelname`
- `<ru.panasyuk.message>` : send a ru.panasyuk.message to all user joined channel
- `logout` : logout from current `nickname`
- `exit` : stop program